package Test;

import org.testng.annotations.Test;

public class Login extends TestBase {


    @Test(retryAnalyzer = Retry.class)
    public void Login() {

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .checkPageisCorrect()
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin()
                .checkSuccessLoginAsAdmin();
    }
}
