package Test;

import org.testng.annotations.Test;

public class CreateTemplate extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void createTemplate(){

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();
        main
                .selectMenuTab("New message");

        createindividualmessagepage
                .selectIndividualMessageType()
                .writeSubject("Testing template")
                .writeMessage("Testing done");

        createtemplatepage
                .createTemplatebutton();

        createtemplatepage
                .inputNameofTemplate()
                .confirmCreateTemplate();

        createtemplatepage
                .okWindow();
    }
}
