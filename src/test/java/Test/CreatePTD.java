package Test;


import org.testng.annotations.Test;

public class CreatePTD extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void createPTD() {
        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        main
                .selectMenuTab("Parent-Teacher-Day");

        createptdpage
                .newPTD()
                .selectDate()
                .selectDuration()
                .createPTD()
                .publishPTD()
                .finishpublishPTD();

    }
}
