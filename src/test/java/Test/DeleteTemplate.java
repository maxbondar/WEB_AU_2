package Test;

import org.testng.annotations.Test;

public class DeleteTemplate extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void deleteTemplate() {

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();
        main
                .selectMenuTab("New message");

        createindividualmessagepage
                .selectIndividualMessageType();

        deletetemplatepage
                .loadTemplate()
                .selectTemplate()
                .deleteTemplate();

    }
}
