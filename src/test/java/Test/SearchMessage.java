package Test;

import org.testng.annotations.Test;

public class SearchMessage extends TestBase {


    @Test(retryAnalyzer = Retry.class)
    public void searchMessage() {
        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        searchmessagepage
                .pressFilterbtn()
                .selectNotRead()
                .selectNotConfirmed()
                .removeNotConfirmed()
                .inputSearchfilter()
                .pressSeacrhbtn();
    }
}
