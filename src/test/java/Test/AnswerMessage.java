package Test;

import org.testng.annotations.Test;

public class AnswerMessage extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void answerMessage() {

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .checkPageisCorrect()
                .writeLogin("p.huk")
                .writePassword("1asdfasdf")
                .pressLogin();

        answermessagepage
                .selectNormalMessage()
                .writeMessage("Text of the answer")
                .confirmation()
                .sendAnswer();
    }
}
