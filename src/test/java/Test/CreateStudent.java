package Test;


import org.testng.annotations.Test;

public class CreateStudent extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void createStudent() {

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        main
                .selectMenuTab("Settings");

        registrationstudentpage
                .selectStudentsTab()
                .assignNewStudent()
                .writeClass()
                .writeFirstnameLastname("Autotest", "Person")
                .selectBirthday()
                .selectGender()
                .registerStudent()
                .searchFilter("Person");

    }
}