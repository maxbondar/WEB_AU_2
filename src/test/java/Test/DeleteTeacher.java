package Test;

import org.testng.annotations.Test;

public class DeleteTeacher extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void deleteTeacher() {

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        main
                .selectMenuTab("Settings");

        registrationstudentpage
                .selectStudentsTab();

        createteacherpage
                .selectTeacherTab();

        deleteteacherpage
                .deleteTeacher();

    }
}
