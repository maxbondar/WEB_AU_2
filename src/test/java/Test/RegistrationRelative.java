package Test;


import org.testng.annotations.Test;

public class RegistrationRelative extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void registrationRelative() {

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        main
                .selectMenuTab("Settings");

        registrationstudentpage
                .selectStudentsTab()
                .searchFilter("Person")
                .registrationRelative()
                .inputRelativeInformationAndPassword()
                .checkSuccessRegistration("Father Person");

    }
}
