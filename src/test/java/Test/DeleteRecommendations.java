package Test;

import org.testng.annotations.Test;

public class DeleteRecommendations extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void deleteRecommendations() {
        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        main
                .selectMenuTab("Recommendations");

        deleterecommendationspage
                .deleteRecommendations()
                .confirmDeleting();

    }
}
