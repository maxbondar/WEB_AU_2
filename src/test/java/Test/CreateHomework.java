package Test;

import org.testng.annotations.Test;

public class CreateHomework extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void createHomework() {
        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        main
                .selectMenuTab("Homework");

        createhomeworkpage
                .selectClass()
                .newHomework()
                .selectDue()
                .confirmationRequsted()
                .writeTextHomework("Testing");

        createhomeworkpage
                .saveHomework();

    }
}
