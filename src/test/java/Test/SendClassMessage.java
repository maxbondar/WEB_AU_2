package Test;


import org.testng.annotations.Test;

public class SendClassMessage extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void sendClassMessage() {
        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        main
                .selectMenuTab("New message");

        createclassmessagepage
                .selectClassMessageType()
                .selectClass();

        createindividualmessagepage
                .writeSubject("AutoTestClassMessage")
                .writeMessage("Testing fine!")
                .requestConfirmation()
                .allowFeedback("Freetext");


        createindividualmessagepage
                .sendMessage()
                .checkSuccessSendMessage();
    }
}
