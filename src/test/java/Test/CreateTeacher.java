package Test;

import org.testng.annotations.Test;

public class CreateTeacher extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void createTeacher() {

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        main
                .selectMenuTab("Settings");

        registrationstudentpage
                .selectStudentsTab();

        createteacherpage
                .selectTeacherTab()
                .createNewTeacher()
                .inputTeacherInfo()
                .submitAddTeacher();

        main
                .logout();

        loginpage
                .writeLogin("a.teacher12")
                .writePassword("1asdfasdf")
                .pressLoginNullMessage();

        registrationstudentpage
                .checkSuccessRegistration("Auto Teacher");
    }
}