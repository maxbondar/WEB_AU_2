package Test;

import org.testng.annotations.Test;

public class SendReminder extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void sendMessage() {

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        reminderpage
                .pressReminderBtn()
                .pressOkWindowBtn();

    }
}
