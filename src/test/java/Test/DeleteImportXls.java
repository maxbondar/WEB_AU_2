package Test;

import org.testng.annotations.Test;

import java.awt.*;

public class DeleteImportXls extends TestBase {


    @Test(retryAnalyzer = Retry.class)
    public void deleteImport() throws AWTException, InterruptedException {

        main
                .gotoUAT();

        loginpage
                .checkPageisCorrect()
                .writeLogin("a.dmin")
                .writePassword("GCs5Brj#RHM?")
                .pressLoginNullMessage();

        main
                .selectMenuTabNullMessage("Settings");

        importpage
                .selectImportXls()
                .selectXlsFile("D:\\BM\\import_library\\123123_griech-2.xml");

        importpage
                .chooseBehavior("Update only")
                .pressLoadBtn()
                .openAllViews()
                .confirmImport();

    }
}