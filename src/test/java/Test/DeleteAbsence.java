package Test;

import org.testng.annotations.Test;

public class DeleteAbsence extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void deleteAbsence() {

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .checkPageisCorrect()
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        main
                .selectMenuTab("Absence");

            answerabsencepage
                    .selectAbsenceMessage();

            closeabsencepage
                    .deleteAbsencebutton();

    }
}