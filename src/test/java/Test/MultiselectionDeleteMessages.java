package Test;

import org.testng.annotations.Test;

public class MultiselectionDeleteMessages extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void multiDelete() {

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .checkPageisCorrect()
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();


        multiselectionpage
                .pressEditbtn()
                .chooseMessages(2)
                .pressDeletebtn()
                .confirmDeletebtn();

    }
}
