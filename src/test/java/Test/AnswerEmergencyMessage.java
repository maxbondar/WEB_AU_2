package Test;

import org.testng.annotations.Test;

public class AnswerEmergencyMessage extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void answerEmergencyMessage() {

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .checkPageisCorrect()
                .writeLogin("e.bondarenko1")
                .writePassword("1asdfasdf")
                .pressLogin();

        answeremergencymessagepage
                .selectEmergencyMessage();
        answermessagepage
                .writeMessage("Answer on the emergency")
                .sendAnswer();
    }
}
