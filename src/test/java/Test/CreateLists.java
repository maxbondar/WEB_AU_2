package Test;

import org.testng.annotations.Test;

public class CreateLists extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void createList() {

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        main
                .selectMenuTab("Lists");

        createlistpage
                .selectClass()
                .newList()
                .selectstudentPerson()
                .writeName()
                .writeDescription()
                .selectDue()
                .makeTable();

        createlistpage
                .saveList();

    }
}
