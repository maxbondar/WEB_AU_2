package Test;

import org.testng.annotations.Test;

import java.awt.*;

public class ImportAsXml extends TestBase {


    @Test(retryAnalyzer = Retry.class)
    public void importSchool() throws AWTException, InterruptedException {

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .checkPageisCorrect()
                .writeLogin("a.dmin")
                .writePassword("GCs5Brj#RHM?")
                .pressLoginNullMessage();

        main
                .selectMenuTabNullMessage("Settings");

        importpage
                .selectImportXls()
                .selectXlsFile("D:\\BM\\import_library\\123123_griech-1.xml");

        importpage
                .chooseBehavior("Create and Update")
                .pressLoadBtn()
                .openAllViews()
                .confirmImport();

    }
}