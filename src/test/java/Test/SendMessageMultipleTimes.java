package Test;

import org.testng.annotations.Test;

public class SendMessageMultipleTimes extends SendMessageBaseTest {

    @Test
    public void sendMessagesMultipleTimes() {
        int numberOfTimes = 10;

        for (int i = 0; i < numberOfTimes; i++) {
            sendMessageActions();
        }
    }
}