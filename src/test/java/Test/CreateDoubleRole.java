package Test;

import org.testng.annotations.Test;

public class CreateDoubleRole extends TestBase {

    @Test(retryAnalyzer = Retry.class)
    public void createdoublerole() {

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        main
                .selectMenuTab("Settings");

        registrationstudentpage
                .selectStudentsTab()
                .assignNewStudent()
                .writeClass()
                .writeFirstnameLastname("DoubleRole", "Person")
                .selectBirthday()
                .selectGender();


        registrationstudentpage
                .registerStudent();

        registrationstudentpage
                .searchFilter("DoubleRole");

        createdoublerolepage
                .registrationDoublerole();

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        main
                .selectMenuTab("Settings");

        registrationstudentpage
                .selectStudentsTab();

        deletestudentpage
                .deleteStudent("DoubleRole");

    }
}
