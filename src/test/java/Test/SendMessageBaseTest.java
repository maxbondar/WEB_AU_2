package Test;

public class SendMessageBaseTest extends TestBase{
    public void sendMessageActions(){

        main
                .gotoUAT()
                .checkLoginPage();

        loginpage
                .writeLogin("t.admin3")
                .writePassword("1asdfasdf")
                .pressLogin();

        main
                .selectMenuTab("New message");

        createindividualmessagepage
                .selectIndividualMessageType()
                .selectTypeOfReceiver(1)
                .selectChild()
                .writeSubject("WEB_AutoTestIndividualMessage")
                .writeMessage("Testing fine!")
                .requestConfirmation()
                .allowFeedback("Freetext");

        createindividualmessagepage
                .sendMessage()
                .checkSuccessSendMessage();
    }
}
