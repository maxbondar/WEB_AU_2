package Page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class MutiselectionPage extends BasePage {
    public MutiselectionPage(WebDriver driver) {
        super(driver);
    }

    @Step("Press edit button om message tab")
    public MutiselectionPage pressEditbtn() {
        driver.findElement(By.xpath("//i[@class='fas fa-pen fa-2x']")).click();
        return this;
    }

    @Step("Choose several messages")
    public MutiselectionPage chooseMessages(int number) {

        for (int i = 1; i <= number; i++) {
            driver.findElement(By.xpath("(//input[@ng-show=\"showCheckboxes\"])[" + i + "]")).click();
        }

        return this;
    }

    @Step("Press delete button  ")
    public MutiselectionPage pressDeletebtn() {
        driver.findElement(By.xpath("//button[@ng-click=\"deleteCheckedMessages()\"]")).click();
        return this;
    }

    @Step("Press confirm button")
    public MutiselectionPage confirmDeletebtn() {
        driver.findElement(By.xpath("//button[@ng-click=\"dialog.hide()\"]")).click();
        wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath("//button[@ng-click=\"dialog.hide()\"]"))));
        return this;
    }
}
