package Page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.testng.Assert.assertTrue;

public class Main extends BasePage {

    String UAT_URL = "https://schoolupdate-bitmedia.test.s.bitmedia.at/schulupdate-efb759645a77b127496a497bdfe88f363d8cd24c/";
    String DEV_URL = "https://schoolupdate-edugroup.dev.s.bitmedia.at/schulupdate-efb759645a77b127496a497bdfe88f363d8cd24c/";

    public Main(WebDriver driver) {
        super(driver);
    }

    @Step("Open the webapp")
    public Main gotoUAT() {
        driver.get(UAT_URL);
        return this;
    }

    @Step("Open the webapp")
    public Main gotoDEV() {
        driver.get(DEV_URL);
        return this;
    }

    @Step("Check login page")
    public Main checkLoginPage() {
        assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'Login')]")).isDisplayed());
        assertTrue(driver.findElement(By.xpath("//label[contains(text(),'Username')]")).isDisplayed());
        assertTrue(driver.findElement(By.xpath("//label[contains(text(),'Password')]")).isDisplayed());
        assertTrue(driver.findElement(By.xpath("//button[@id='loginBtn']")).isDisplayed());
        return this;
    }

    @Step("Logout")
    public Main logout() {
        driver.findElement(By.xpath("//button[@ng-click='logout()']")).click();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[text()='Login']"))));
        return this;
    }

    public Main okAlert() {
        try {
            driver.switchTo().alert().accept();
        } catch (NoAlertPresentException e) {
        }
        driver.switchTo().defaultContent();
        return this;
    }


}