package Page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class DeleteTemplatePage extends BasePage {
    public DeleteTemplatePage(WebDriver driver) {
        super(driver);
    }

    @Step("Press load template button")
    public DeleteTemplatePage loadTemplate() {
        driver.findElement(By.xpath("//button[@ng-click=\"showLoadFromTemplateDialog($event)\"]")).click();
        return this;
    }

    @Step("Search and select template")
    public DeleteTemplatePage selectTemplate() {
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='list-container']"))));
        driver.findElement(By.xpath("//span[@class=\"select2-arrow ui-select-toggle\"]")).click();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class=\"select2-result-label ui-select-choices-row-inner\"]"))));
        driver.findElement(By.xpath("//div[@class=\"select2-result-label ui-select-choices-row-inner\"]")).click();
        return this;
    }

    @Step("delete template button")
    public DeleteTemplatePage deleteTemplate() {
        driver.findElement(By.xpath("//button[@ng-click=\"delete()\"]")).click();
        return this;
    }
}
