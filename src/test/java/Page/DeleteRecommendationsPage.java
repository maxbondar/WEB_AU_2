package Page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class DeleteRecommendationsPage extends BasePage {
    public DeleteRecommendationsPage(WebDriver driver) {
        super(driver);
    }

    @Step("Delete recommendations")
    public DeleteRecommendationsPage deleteRecommendations() {
        driver.findElement(By.xpath("(//*[text()='Autotest']/../descendant::button)[2]")).click();
        return this;
    }

    @Step("confirm deleting")
    public DeleteRecommendationsPage confirmDeleting() {
        driver.findElement(By.xpath("//button[@class='md-primary md-confirm-button md-button md-ink-ripple md-default-theme']")).click();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@ng-show=\"successmsg.text\"]"))));
        return this;
    }
}
