package Page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class DeleteHomeworkPage extends BasePage {
    public DeleteHomeworkPage(WebDriver driver) {
        super(driver);
    }

    @Step("Delete homework")
    public DeleteHomeworkPage deleteHomework() {
        driver.findElement(By.xpath("//button[@ng-click='deleteHomework(row.homework, hw.id)']")).click();
        return this;
    }

    @Step("Confirm deleting")
    public DeleteHomeworkPage confirmDeleting() {
        driver.findElement(By.xpath("//button[@class='md-primary md-confirm-button md-button md-ink-ripple md-default-theme']")).click();
        wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath("//p[text()='Testing']"))));
        return this;
    }
}
