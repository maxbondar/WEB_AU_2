package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

public class ImportPage extends BasePage {
    public ImportPage(WebDriver driver) {
        super(driver);
    }

    public ImportPage selectImportXls() {
        driver.findElement(By.xpath("//button[@ui-sref=\"root.importXml\"]")).click();
        return this;
    }

    public ImportPage selectXlsFile(String path) throws AWTException, InterruptedException {
        driver.findElement(By.xpath("//button[@ng-model=\"file\"]")).click();

        Thread.sleep(1500);

        Robot rb = new Robot();

        StringSelection str = new StringSelection(path);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);

        rb.keyPress(KeyEvent.VK_CONTROL);
        rb.keyPress(KeyEvent.VK_V);

        rb.keyRelease(KeyEvent.VK_CONTROL);
        rb.keyRelease(KeyEvent.VK_V);

        rb.keyPress(KeyEvent.VK_ENTER);
        rb.keyRelease(KeyEvent.VK_ENTER);

        return this;
    }

    public ImportPage chooseBehavior(String command){
        driver.findElement(By.xpath("//select[@ng-model=\"formData.behavior\"]")).click();

        switch (command){
            case "Create or Update":
                driver.findElement(By.xpath("//option[@value=\"CREATE_OR_UPDATE\"]")).click();
                break;
            case "Update only":
                driver.findElement(By.xpath("//option[@value=\"REPLACE\"]")).click();
                break;
            case "Create only":
                driver.findElement(By.xpath("//option[@value=\"APPEND\"]")).click();
                break;
            case "Force Create or Update":
                driver.findElement(By.xpath("//option[@value=\"FORCE_CREATE_OR_UPDATE\"]")).click();
                break;
        }
        return this;
    }

    public ImportPage pressLoadBtn() {
        driver.findElement(By.xpath("//button[@ng-click=\"loadStep()\"]")).click();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h4[contains(text(),'Step 2')]"))));
        return this;
    }

    public ImportPage openAllViews() {
        driver.findElement(By.xpath("//button[@class=\"accordion\"][1]")).click();
        driver.findElement(By.xpath("//button[@class=\"accordion\"][1]")).click();
        driver.findElement(By.xpath("//button[@class=\"accordion\"][1]")).click();
        return this;
    }

    public ImportPage confirmImport() {
        driver.findElement(By.xpath("//button[@ng-click=\"importStep()\"]")).click();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h4[contains(text(),'Step 3')]"))));
        return this;
    }


}
