package Page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BasePage {

    public WebDriver driver;
    public WebDriverWait wait;

    //constructor
    public BasePage(WebDriver driver) {this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));}

    //Wait method
    public void waitVisibility(By elementBy) {
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementBy));
    }


    //Click method
    public void click(By elementBy) {
        waitVisibility(elementBy);
        driver.findElement(elementBy).click();
    }

    //Is element displayed
    public void isElementDisplayed(By elementBy) {
        waitVisibility(elementBy);
        assert (driver.findElement(elementBy).isDisplayed());
    }

    @Step("Select tab menu")
    public void selectMenuTab(String nametab) {
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//ul[@class='messages']"))));
        driver.findElement(By.xpath("//span[text()='" + nametab + "']")).click();
    }

    @Step("Select tab menu")
    public void selectMenuTabNullMessage(String nametab) {
        driver.findElement(By.xpath("//span[text()='" + nametab + "']")).click();
    }

}