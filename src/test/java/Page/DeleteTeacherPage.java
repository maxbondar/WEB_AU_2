package Page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class DeleteTeacherPage extends BasePage {
    public DeleteTeacherPage(WebDriver driver) {
        super(driver);
    }

    @Step("Delete teacher")
    public DeleteTeacherPage deleteTeacher() {
        driver.findElement(By.xpath("//div[text()='Auto']")).click();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h3[contains(text(),'Edit teacher')]"))));
        driver.findElement(By.xpath("//input[@id='chkActivateDeleteButton']")).click();
        driver.findElement(By.xpath("//button[@ng-click='deleteTeacher()']")).click();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h3[contains(text(),'Edit teacher')]"))));
        return this;
    }
}
