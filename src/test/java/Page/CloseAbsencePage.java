package Page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CloseAbsencePage extends BasePage {
    public CloseAbsencePage(WebDriver driver) {
        super(driver);
    }

    @Step("Close absence message")
    public CloseAbsencePage closeAbsencebutton() {
        driver.findElement(By.xpath("//button[@ng-click='showCloseAbsence(message.von)']")).click();
        return this;
    }

    @Step("Delete absence message")
    public CloseAbsencePage deleteAbsencebutton() {
        driver.findElement(By.xpath("//i[@class='fa fa-trash']")).click();
        driver.findElement(By.xpath("//button[@class='md-primary md-confirm-button md-button md-ink-ripple md-default-theme']")).click();
        wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath("//md-dialog-content"))));
        return this;
    }

    @Step("Press Ok on window")
    public CloseAbsencePage selectOkonwindow() {
        driver.findElement(By.xpath("//button[@ng-click='ok()']")).click();
        wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath("//h2[contains(.,'Close Absence')]"))));
        return this;
    }
}
