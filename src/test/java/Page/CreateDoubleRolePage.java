package Page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CreateDoubleRolePage extends BasePage {
    public CreateDoubleRolePage(WebDriver driver) {
        super(driver);
    }

    @Step("Registration relative")
    public CreateDoubleRolePage registrationDoublerole() {
        String regCode = driver.findElement(By.xpath("(//div[@role=\"gridcell\"])[7]//span")).getText();
        driver.findElement(By.xpath("//button[@ng-click='logout()']")).click();
        driver.findElement(By.xpath("(//button[@class='btn btn-small btn-primary btn-block'])[2]")).click();
        driver.findElement(By.xpath("//input")).sendKeys("" + regCode + "");
        driver.findElement(By.xpath("//button[@class=\"primary-button-registration\"]")).click();
        driver.findElement(By.xpath("(//input[@type=\"radio\"])[2]")).click();
        driver.findElement(By.xpath("//button[@class=\"reg-button\"]")).click();
        driver.findElement(By.xpath("//input[@name=\"username\"]")).sendKeys("a.teacher12");
        driver.findElement(By.xpath("(//input[@name=\"password\"])[3]")).sendKeys("1asdfasdf");
        driver.findElement(By.xpath("//button[@ng-click=\"submitAssignChildForm()\"]")).click();
        return this;
    }
}
