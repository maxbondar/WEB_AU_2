package Page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class DeleteListsPage extends BasePage {
    public DeleteListsPage(WebDriver driver) {
        super(driver);
    }

    @Step("Delete list")
    public DeleteListsPage deleteList() {
        driver.findElement(By.xpath("//button[@ng-click='deleteList(row.lists, list.id)']")).click();
        return this;
    }

    @Step("Enter OK on window message")
    public DeleteListsPage confirmDeleting() {
        driver.findElement(By.xpath("//button[@class='md-primary md-confirm-button md-button md-ink-ripple md-default-theme']")).click();
        wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath("//b[contains(text(),'Autotest lists')]"))));
        return this;
    }
}
