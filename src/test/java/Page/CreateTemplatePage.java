package Page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CreateTemplatePage extends BasePage {
    public CreateTemplatePage(WebDriver driver) {
        super(driver);
    }


    @Step("Press create template button")
    public CreateTemplatePage createTemplatebutton() {
        driver.findElement(By.xpath("//button[@ng-click=\"showCreateMessageTemplateDialog($event)\"]")).click();
        return this;
    }

    @Step("Input template name")
    public CreateTemplatePage inputNameofTemplate() {
        driver.findElement(By.xpath("//input[@placeholder=\"Template name\"]")).sendKeys("Name of Test template");
        return this;
    }

    @Step("Confirm creating template")
    public CreateTemplatePage confirmCreateTemplate() {
        driver.findElement(By.xpath("//button[@ng-click=\"dialog.hide()\"]")).click();
        return this;
    }

    @Step("Press ok on window")
    public CreateTemplatePage okWindow() {
        driver.findElement(By.xpath("//button[@ng-click=\"dialog.hide()\"]")).click();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[text()='Created']"))));
        return this;
    }

}
