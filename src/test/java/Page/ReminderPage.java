package Page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ReminderPage extends BasePage {
    public ReminderPage(WebDriver driver) {
        super(driver);
    }

    @Step("Press reminder button")
    public ReminderPage pressReminderBtn() {
        driver.findElement(By.xpath("//a[@ng-click=\"createReminder(message.id)\"]")).click();
        return this;
    }


    @Step("Confirm sending reminder")
    public ReminderPage pressOkWindowBtn() {
        driver.findElement(By.xpath("//button[@ng-click=\"confirm()\"]")).click();
        wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath("//md-dialog-content"))));
        return this;
    }
}
