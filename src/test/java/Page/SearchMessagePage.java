package Page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SearchMessagePage extends BasePage{
    public SearchMessagePage(WebDriver driver) {
        super(driver);
    }

    @Step("")
    public SearchMessagePage pressFilterbtn() {
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//i[@class='fas fa-search fa-2x']"))));
        driver.findElement(By.xpath("//i[@class='fas fa-search fa-2x']")).click();
        return this;
    }

    @Step("")
    public SearchMessagePage selectNotRead() {
        driver.findElement(By.xpath("//input[@ng-value=\"'filter_not_read'\"]")).click();
        return this;
    }

    @Step("")
    public SearchMessagePage selectNotConfirmed() {
        driver.findElement(By.xpath("//input[@ng-value=\"'notConfirmed'\"]")).click();
        return this;
    }

    @Step("")
    public SearchMessagePage removeNotConfirmed() {
        driver.findElement(By.xpath("//input[@ng-value=\"'notConfirmed'\"]")).click();
        return this;
    }

    @Step("")
    public SearchMessagePage inputSearchfilter() {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("document.getElementsByClassName('searchField')[0].setAttribute('value', 'Bondar')");
        return this;
    }

    @Step("")
    public SearchMessagePage pressSeacrhbtn() {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("document.getElementsByClassName('secondary-button')[0].click();");
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@class='message-header ng-binding']"))));
        return this;
    }


}
